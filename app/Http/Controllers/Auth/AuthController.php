<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use App\Models\entrepreneurship;
use App\Models\entrepreneurship_types;
use App\Models\Media;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected function create_user(Request $request) {
        $data=$request->all();

        $user = User::create([
            'name' => $data['username'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $entrepreneurship = $this->create_entrepreneurship($request,$user);
    }

    protected function create_entrepreneurship(Request $request, $user) {
        $data=$request->all();

        // $validator = $this->validate($request, [
        //     'e_name' => 'required',
        //     'description' => 'required',
        //     'category' => 'required',
        //     'city' => 'required',
        //     'commune' => 'required',
        //     'phone' => 'required|max:50',
        //     'e_email' => 'required|email|max:255',
        //     'image' => 'mimes:jpeg,bmp,png,jpg'
        // ]);

        $entre = entrepreneurship::create([
            'user_id' => $user['id'],
            'name' => $data['e_name'],
            'description' => $data['description'],
            'category_id' => $data['category'],
            'city_id' => $data['city'],
            'commune_id' => $data['commune'] == "0" ? null : $data['commune'], 
            'phone' => $data['phone'],
            'email' => $data['e_email'],
            'address' => $data['address'],
            'social_media' => $data['social_web']
        ]);

        if(isset($data['type'])){
            $this->register_types($entre['id'], $data['type']);
        }

        if(isset($data['image'])){
            $this->upload_image($request, $user['id'], $entre['id']);
        }

        Auth::login($user);

    }

    private function upload_image(Request $request, $user, $entre){
        $imageName = $user . '_' . $request->file('image')->getFilename() . '.' . $request->file('image')->getClientOriginalExtension();

        $request->file('image')->move(
            base_path() . '/public/uploads/images/', $imageName
        );

        $image = Media::create([
            "name"  => $imageName,
            "path"  => 'uploads/images/',
            "size"  => '0',
            "type"  => $request->file('image')->getClientOriginalExtension()
        ]);

        $entre = entrepreneurship::find($entre);
        $entre['image_id'] = $image['id'];
        $entre->save();

        return $image;
    }

    private function register_types($entre_id, $typeids)
    {
        $type = entrepreneurship_types::where('entrepreneurship_id', '=', $entre_id)->delete();
        for ($i=0; $i < count($typeids); $i++) { 
            $type = entrepreneurship_types::create([
                'type_id' => $typeids[$i],
                'entrepreneurship_id' => $entre_id
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validate($request, [
            'username' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:5',
            'e_name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'city' => 'required',
            'phone' => 'required|max:50',
            'e_email' => 'required|email|max:255',
            'image' => 'mimes:jpeg,bmp,png,jpg'
        ]);

        $user=$this->create_user($request);
        return redirect($this->redirectPath());
        
    }

    public function getRegister()
    {

        $cats = categories::all();
        $typs = types::all();
        $cits = cities::all();
        $coms = communes::all();
        return view('auth.register')->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
    }


    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginPath()
    {
        return route('entrepreneurship');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {

        return route('entrepreneurship');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    
    protected $redirecTo='/library';
    public function __construct()
    {
        $this->middleware('guest');
    }
     /**
         * Display the form to request a password reset link.
         *
         * @return \Illuminate\Http\Response
         */
    public function getEmail()
    {
        $cats = categories::all();
        $typs = types::all();
        $cits = cities::all();
        $coms = communes::all();

        return view('auth.password')->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
    }

     /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset($token = null)
    {
        $cats = categories::all();
        $typs = types::all();
        $cits = cities::all();
        $coms = communes::all();

        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('auth.reset')->with('token', $token)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
    }


    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

}

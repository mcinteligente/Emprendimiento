<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecategoriesRequest;
use App\Http\Requests\UpdatecategoriesRequest;
use App\Libraries\Repositories\categoriesRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use App\helpers\Helper as Helper;
use Response;
use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use Illuminate\Support\Facades\Auth;

class categoriesController extends AppBaseController
{

	/** @var  categoriesRepository */
	private $categoriesRepository;

	function __construct(categoriesRepository $categoriesRepo)
	{
		$this->categoriesRepository = $categoriesRepo;
	}

	/**
	 * Display a listing of the categories.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();

			$categories = $this->categoriesRepository->paginate(10);

			return view('categories.index')->with('categories', $categories)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}

		else{

			return redirect(route('entrepreneurships.index'));

		}
	}

	/**
	 * Show the form for creating a new categories.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('categories.create');
	}

	/**
	 * Store a newly created categories in storage.
	 *
	 * @param CreatecategoriesRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecategoriesRequest $request)
	{
		

		$input = $request->all();
		$input['slug']=Helper::makeSlug($input['name']);
		$categories = $this->categoriesRepository->create($input);

		Flash::success('Categoría guardada correctamente.');

		return redirect(route('poderoso.categories.index'));
	}

	/**
	 * Display the specified categories.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$categories = $this->categoriesRepository->find($id);

		if(empty($categories))
		{
			Flash::error('Categoría no encontrada');

			return redirect(route('poderoso.categories.index'));
		}

		return view('categories.show')->with('categories', $categories);
	}

	/**
	 * Show the form for editing the specified categories.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();

			$categories = $this->categoriesRepository->find($id);

			if(empty($categories))
			{
				Flash::error('Categoría no encontrada');

				return redirect(route('poderoso.categories.index'));
			}

			return view('categories.edit')->with('categories', $categories)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}
	}

	/**
	 * Update the specified categories in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecategoriesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecategoriesRequest $request)
	{
		$categories = $this->categoriesRepository->find($id);

		if(empty($categories))
		{
			Flash::error('Categoría no encontrada');

			return redirect(route('poderoso.categories.index'));
		}
		$req = $request->all();
		$req['slug']=Helper::makeSlug($req['name']);
		$categories = $this->categoriesRepository->updateRich($req, $id);

		Flash::success('Categoría actualizada correctamente.');

		return redirect(route('poderoso.categories.index'));
	}

	/**
	 * Remove the specified categories from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Auth::user())
		{
			$categories = $this->categoriesRepository->find($id);

			if(empty($categories))
			{
				Flash::error('Categoría no encontrada');

				return redirect(route('poderoso.categories.index'));
			}

			$this->categoriesRepository->delete($id);

			Flash::success('Categoría eliminada correctamente');

			return redirect(route('poderoso.categories.index'));
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}
	}	
}

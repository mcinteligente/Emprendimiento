<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecitiesRequest;
use App\Http\Requests\UpdatecitiesRequest;
use App\Libraries\Repositories\citiesRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use App\helpers\Helper as Helper;
use Response;
use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use Illuminate\Support\Facades\Auth;

class citiesController extends AppBaseController
{

	/** @var  citiesRepository */
	private $citiesRepository;

	function __construct(citiesRepository $citiesRepo)
	{
		$this->citiesRepository = $citiesRepo;
	}

	/**
	 * Display a listing of the cities.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();

			$cities = $this->citiesRepository->paginate(10);

			return view('cities.index')->with('cities', $cities)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}		
	}

	/**
	 * Show the form for creating a new cities.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('cities.create');
	}

	/**
	 * Store a newly created cities in storage.
	 *
	 * @param CreatecitiesRequest $request
	 *
	 * @return Response
	 */
	
	public function store(CreatecitiesRequest $request)
	{
		

		$input = $request->all();
		$input['slug']=Helper::makeSlug($input['name']);
		$cities = $this->citiesRepository->create($input);

		Flash::success('Ciudad guardada correctamente.');

		return redirect(route('poderoso.cities.index'));
	}


	/**
	 * Display the specified cities.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$cities = $this->citiesRepository->find($id);

		if(empty($cities))
		{
			Flash::error('Ciudad no encontrada.');

			return redirect(route('poderoso.cities.index'));
		}

		return view('cities.show')->with('cities', $cities);
	}

	/**
	 * Show the form for editing the specified cities.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();
			$cities = $this->citiesRepository->find($id);

			if(empty($cities))
			{
				Flash::error('Ciudad no encontrada.');

				return redirect(route('poderoso.cities.index'));
			}

			return view('cities.edit')->with('cities', $cities)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}	
		else{

			return redirect(route('entrepreneurships.index'));

		}
	}

	/**
	 * Update the specified cities in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecitiesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecitiesRequest $request)
	{
		$cities = $this->citiesRepository->find($id);

		if(empty($cities))
		{
			Flash::error('Ciudad no encontrada.');

			return redirect(route('poderoso.cities.index'));
		}
		$req = $request->all();
		$req['slug']=Helper::makeSlug($req['name']);
		$cities = $this->citiesRepository->updateRich($req, $id);

		Flash::success('Ciudad actualizada correctamente');

		return redirect(route('poderoso.cities.index'));
	}

	/**
	 * Remove the specified cities from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Auth::user())
		{
			$cities = $this->citiesRepository->find($id);

			if(empty($cities))
			{
				Flash::error('Ciudad no encontrada.');

				return redirect(route('poderoso.cities.index'));
			}

			$this->citiesRepository->delete($id);

			Flash::success('Ciudad eliminada correctamente');

			return redirect(route('poderoso.cities.index'));
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}	
	}
}

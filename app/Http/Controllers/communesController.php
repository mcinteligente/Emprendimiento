<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecommunesRequest;
use App\Http\Requests\UpdatecommunesRequest;
use App\Libraries\Repositories\communesRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use App\helpers\Helper as Helper;
use Response;
use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use Illuminate\Support\Facades\Auth;

class communesController extends AppBaseController
{

	/** @var  communesRepository */
	private $communesRepository;

	function __construct(communesRepository $communesRepo)
	{
		$this->communesRepository = $communesRepo;
	}

	/**
	 * Display a listing of the communes.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();
	        
			$communes = $this->communesRepository->paginate(10);

			return view('communes.index')->with('communes', $communes)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}	
	}

	/**
	 * Show the form for creating a new communes.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('communes.create');
	}

	/**
	 * Store a newly created communes in storage.
	 *
	 * @param CreatecommunesRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecommunesRequest $request)
	{
		$input = $request->all();
		$input['slug']=Helper::makeSlug($input['name']);

		$communes = $this->communesRepository->create($input);

		Flash::success('Comuna guardada correctamente.');

		return redirect(route('poderoso.communes.index'));
	}

	/**
	 * Display the specified communes.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$communes = $this->communesRepository->find($id);

		if(empty($communes))
		{
			Flash::error('Comuna no encontrada.');

			return redirect(route('poderoso.communes.index'));
		}

		return view('communes.show')->with('communes', $communes);
	}

	/**
	 * Show the form for editing the specified communes.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();

			$communes = $this->communesRepository->find($id);

			if(empty($communes))
			{
				Flash::error('Comuna no encontrada.');

				return redirect(route('poderoso.communes.index'));
			}

			return view('communes.edit')->with('communes', $communes)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}	
	}

	/**
	 * Update the specified communes in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecommunesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecommunesRequest $request)
	{
		$communes = $this->communesRepository->find($id);

		if(empty($communes))
		{
			Flash::error('Comuna no encontrada.');

			return redirect(route('poderoso.communes.index'));
		}

		$req = $request->all();
		$req['slug']=Helper::makeSlug($req['name']);
		
		$communes = $this->communesRepository->updateRich($req, $id);

		Flash::success('Comuna actualizada correctamente.');

		return redirect(route('poderoso.communes.index'));
	}

	/**
	 * Remove the specified communes from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Auth::user())
		{

			$communes = $this->communesRepository->find($id);

			if(empty($communes))
			{
				Flash::error('Comuna no encontrada.');

				return redirect(route('poderoso.communes.index'));
			}

			$this->communesRepository->delete($id);

			Flash::success('Comuna eliminada correctamente');

			return redirect(route('poderoso.communes.index'));
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}		
	}
}

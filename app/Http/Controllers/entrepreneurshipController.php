<?php namespace App\Http\Controllers;

use App\User;
use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use App\Models\entrepreneurship_types;
use App\Models\entrepre_types;
use App\Models\entrepreneurship;
use App\Models\Media;
use App\Http\Requests;
use App\Http\Requests\CreateentrepreneurshipRequest;
use App\Http\Requests\UpdateentrepreneurshipRequest;
use App\Libraries\Repositories\entrepreneurshipRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Auth;

use Validator;

class entrepreneurshipController extends AppBaseController
{

	/** @var  entrepreneurshipRepository */
	private $entrepreneurshipRepository;

	function __construct(entrepreneurshipRepository $entrepreneurshipRepo)
	{
		$this->entrepreneurshipRepository = $entrepreneurshipRepo;
	}

	/**
	 * Display a listing of the entrepreneurship.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cats = categories::all();
		$typs = types::all();
		$cits = cities::all();
		$coms = communes::all();
		$entrepreneurships = entrepreneurship::where('publish','=','1')->orderBy('created_at', 'desc')->take(4)->get();

		foreach ($entrepreneurships as $key => $value) {
			$value['category'] = $value->categories['name'];
			$value['city'] = $value->cities['name'];
			$value['commune'] = $value->communes['name'];
			$value['owner'] = $value->user;
			$value['image'] = $value->media['path'] . $value->media['name'];
			$value['short_description'] = str_limit($value['description'],150);
			if($value['social_media'] && $value['social_media'] != ''){
				$parsed = parse_url($value['social_media']);
				if (empty($parsed['scheme'])) {
					$value['social_media'] = 'http://' . ltrim($value['social_media'], '/');
				}
			}
		}
		return View('entrepreneurships.index', compact(['entrepreneurships']))->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
	}

	public function admin()
	{
		if (Auth::user())
		{
			$cats = categories::all();
			$typs = types::all();
			$cits = cities::all();
			$coms = communes::all();
	        $types = entrepreneurship_types::all();
	        $entrepreneurships = $this->entrepreneurshipRepository->paginate(10);
	        foreach ($entrepreneurships as $key => $value) {
	            $value['category'] = $value->categories['name'];
	            $value['city'] = $value->cities['name'];
	            $value['commune'] = $value->communes['name'];
	            $value['type'] = $types;
	            $value['short_description'] = str_limit($value['description'],50);
	        }
    
        // var_dump($entrepreneurships);
        	return View('entrepreneurships.admin', compact(['entrepreneurships']))->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
    	}
    	else{

		return redirect(route('entrepreneurships.index'));

		}
	}

	/**
	 * Display a listing of the entrepreneurship.
	 *
	 * @return Response
	 */
	public function paginate(Request $request)
	{
		$cats = categories::all();
		$typs = types::all();
		$cits = cities::all();
		$coms = communes::all();
		$term = $request->get('term') ? $request->get('term') : '';
		$entrepreneurships = entrepreneurship::where('publish','=','1')->term($request->get('term'))->orderBy('created_at', 'desc')->paginate(5);
		foreach ($entrepreneurships as $key => $value) {
			$value['category'] = $value->categories['name'];
			$value['city'] = $value->cities['name'];
			$value['commune'] = $value->communes['name'];
			$value['owner'] = $value->user;
			$value['image'] = $value->media['path'] . $value->media['name'];
			$value['short_description'] = str_limit($value['description'],150);
			if($value['social_media'] && $value['social_media'] != ''){
				$parsed = parse_url($value['social_media']);
				if (empty($parsed['scheme'])) {
					$value['social_media'] = 'http://' . ltrim($value['social_media'], '/');
				}
			}
		}
		return View('entrepreneurships.viewall', compact(['entrepreneurships', 'term']))->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
	}

    public function getCreate()
    {
        $cats = categories::all();
        $typs = types::all();
        $cits = cities::all();
        $coms = communes::all();
        return  view('entrepreneurships.create')->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
        
    }


	protected function create_entrepreneurship(Request $request,$id_register) {
		$data=$request->all();
		$validator=Validator::make($data, [
		    'description' => 'required',
		    'category' => 'required',
		    'phone' => 'required|max:50',
		]);

		if ($validator->fails()) {
		    $this->throwValidationException(
		        $request, $validator
		    );
		}

		$entrepr= entrepreneurships::create([
		    'id' => $id_register,
		    'name' => $data['name'],
		    'description' => $data['description'],
		    'category_id' => $data['category'],
		    'phone' => $data['phone'],
		    'address' => $data['address'],
		]);

      return $entrepr;
    }


	/**
	 * Show the form for creating a new entrepreneurship.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('entrepreneurships.create');
	}

	/**
	 * Store a newly created entrepreneurship in storage.
	 *
	 * @param CreateentrepreneurshipRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateentrepreneurshipRequest $request)
	{
		$input = $request->all();

		$entrepreneurship = $this->entrepreneurshipRepository->create($input);

		Flash::success('Emprendimiento guardado correctamente.');

		return redirect(route('entrepreneurships.index'));
	}

	/**
	 * Display the specified entrepreneurship.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$entrepreneurship = $this->entrepreneurshipRepository->find($id);

		if(empty($entrepreneurship))
		{
			Flash::error('Emprendimiento no encontrado.');

			return redirect(route('entrepreneurships.index'));
		}

		return view('entrepreneurships.show')->with('entrepreneurship', $entrepreneurship);
	}

	/**
	 * Show the form for editing the specified entrepreneurship.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $id)
	{
		if (Auth::user())
		{
	 		$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();
	        
			$entrepreneurship = entrepreneurship::find($id);
			$entrepreneurship['category'] = $entrepreneurship->categories['id'];
			$entrepreneurship['city'] = $entrepreneurship->cities['id'];
			$entrepreneurship['commune'] = $entrepreneurship->communes['id'];
			$entrepreneurship['image'] = url() . '/' . $entrepreneurship->media['path'] . $entrepreneurship->media['name'];
				

			if(empty($entrepreneurship))
			{
				Flash::error('Emprendimiento no encontrado.');

				return redirect(route('entrepreneurships.index'));
			}

		//$infouser=$request->user()->id;
		//$infoid=$entrepreneurship['id'];

			return view('entrepreneurships.edit')->with('entrepreneurship', $entrepreneurship)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

		return redirect(route('entrepreneurships.index'));

		}
	}

	/**
	 * Update the specified entrepreneurship in storage.
	 *
	 * @param  int              $id
	 * @param UpdateentrepreneurshipRequest $request
	 *
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$data=$request->all();
		$validator=Validator::make($data, [
            'e_name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'city' => 'required',
            'commune' => 'required',
            'phone' => 'required|max:50',
            'email' => 'required|email|max:255',
            'image' => 'mimes:jpeg,bmp,png,jpg'
        ]);

        if ($validator->fails()) {
		    $this->throwValidationException(
		        $request, $validator
		    );
		}

		$entrepreneurship = entrepreneurship::find($id);
		
		$data = $request->all();
		$datauser=$request->user();


		$infouser=$request->user()->id;
		$infoid=$entrepreneurship['id'];

		if(isset($data['image'])){
            $this->upload_image($request, $infouser, $infoid);
        }

		$entrepreneurship['name'] = $data['e_name'];
		$entrepreneurship['description'] = $data['description'];
		$entrepreneurship['category_id'] = $data['category'];
		$entrepreneurship['city_id'] = $data['city'];
		$entrepreneurship['commune_id'] = $data['commune'];
		$entrepreneurship['phone'] = $data['phone'];
		$entrepreneurship['email'] = $data['email'];
		$entrepreneurship['address'] = $data['address'];
		$entrepreneurship['social_media'] = $data['social_web'];


	    $entrepreneurship->save($entrepreneurship->toArray());


		Flash::success('Emprendimiento actualizado correctamente.');

		return redirect(route('profile'));
	}

	public function upload_image(Request $request, $user, $entre){
	        $imageName = $user . '_' . $request->file('image')->getFilename() . '.' . $request->file('image')->getClientOriginalExtension();

	        $request->file('image')->move(
	            base_path() . '/public/uploads/images/', $imageName
	        );

	        $image = Media::create([
	            "name"  => $imageName,
	            "path"  => 'uploads/images/',
	            "size"  => '0',
	            "type"  => $request->file('image')->getClientOriginalExtension()
	        ]);

	        $entre = entrepreneurship::find($entre);
	        $entre['image_id'] = $image['id'];
	        $entre->save();

	        return $image;
	}

	public function postEdit(Request $request)
    {
    	if (Auth::user())
		{

	        $infouser=$request->user()->id;
	        $cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();
			$entrepreneurship = entrepreneurship::where('user_id','=', $infouser)->first();
	        if($entrepreneurship){
				$entrepreneurship['category'] = $entrepreneurship->categories['id'];
				$entrepreneurship['city'] = $entrepreneurship->cities['id'];
				$entrepreneurship['commune'] = $entrepreneurship->communes['id'];
				$entrepreneurship['image'] = url() . '/' . $entrepreneurship->media['path'] . $entrepreneurship->media['name'];
	        }

			return view('entrepreneurships.edit')->with('entrepreneurship', $entrepreneurship)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
        }
        else{

		return redirect(route('entrepreneurships.index'));

		}
    }

    public function removeAccount(Request $request)
    {
    	if (Auth::user())
		{
	        $user = $request->user();
	        $currentUser = User::find($user->id);
	        $currentUser->delete();

	        return redirect(route('entrepreneurships.index'));
	    }    
	    else{

			return redirect(route('entrepreneurships.index'));
		}
    }
	
	/**
	 * Remove the specified entrepreneurship from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	
	public function destroy($id)
	{
		$entrepreneurship = $this->entrepreneurshipRepository->find($id);
		

		if(empty($entrepreneurship))
		{
			Flash::error('Emprendimiento no encontrado.');

			return redirect()->to('poderoso');
		}
		$iduser=$entrepreneurship['user_id'];
		$usermodel = User::find($iduser);
		$usermodel->delete();

		Flash::success('Emprendimiento eliminado correctamente.');

		return redirect()->to('poderoso');
	}


	/**
	 * Remove the specified entrepreneurship from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function delete_user($id)
	{
		$entrepreneurship = $this->entrepreneurshipRepository->find($id);
		

		if(empty($entrepreneurship))
		{
			Flash::error('Emprendimiento no encontrado.');

			return redirect(route('entrepreneurships.index'));
		}
		$iduser=$entrepreneurship['user_id'];
		$usermodel = User::find($iduser);
		$usermodel->forceDelete();

		$this->logout();
		// $this->entrepreneurshipRepository->delete($id);

		Flash::success('Emprendimiento eliminado correctamente.');

		return redirect(route('entrepreneurships.index'));
	}

	public function terms()
	{
		$cats = categories::all();
        $typs = types::all();
        $cits = cities::all();
        $coms = communes::all();
        return  view('terms')->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
	}
}

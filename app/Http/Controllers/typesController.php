<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatetypesRequest;
use App\Http\Requests\UpdatetypesRequest;
use App\Libraries\Repositories\typesRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use App\helpers\Helper as Helper;
use Response;
use App\Models\categories;
use App\Models\types;
use App\Models\cities;
use App\Models\communes;
use Illuminate\Support\Facades\Auth;

class typesController extends AppBaseController
{

	/** @var  typesRepository */
	private $typesRepository;

	function __construct(typesRepository $typesRepo)
	{
		$this->typesRepository = $typesRepo;
	}

	/**
	 * Display a listing of the types.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();

			$types = $this->typesRepository->paginate(10);

			return view('types.index')->with('types', $types)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}		
	}

	/**
	 * Show the form for creating a new types.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('types.create');
	}

	/**
	 * Store a newly created types in storage.
	 *
	 * @param CreatetypesRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatetypesRequest $request)
	{
		$input = $request->all();
		$input['slug']=Helper::makeSlug($input['name']);
		$types = $this->typesRepository->create($input);

		Flash::success('Tipo de emprendimiento guardado correctamente');

		return redirect(route('poderoso.types.index'));
	}

	/**
	 * Display the specified types.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$types = $this->typesRepository->find($id);

		if(empty($types))
		{
			Flash::error('Tipo de emprendimiento no encontrado');

			return redirect(route('types.index'));
		}

		return view('types.show')->with('types', $types);
	}

	/**
	 * Show the form for editing the specified types.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::user())
		{
			$cats = categories::all();
	        $typs = types::all();
	        $cits = cities::all();
	        $coms = communes::all();

			$types = $this->typesRepository->find($id);

			if(empty($types))
			{
				Flash::error('Tipo de emprendimiento no encontrado');

				return redirect(route('poderoso.types.index'));
			}

			return view('types.edit')->with('types', $types)->with('cats', $cats)->with('typs', $typs)->with('cits', $cits)->with('coms', $coms);
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}		
	}

	/**
	 * Update the specified types in storage.
	 *
	 * @param  int              $id
	 * @param UpdatetypesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatetypesRequest $request)
	{
		$types = $this->typesRepository->find($id);

		if(empty($types))
		{
			Flash::error('Tipo de emprendimiento no encontrado');

			return redirect(route('poderoso.types.index'));
		}

		$req = $request->all();
		$req['slug']=Helper::makeSlug($req['name']);
		$types = $this->typesRepository->updateRich($req, $id);

		Flash::success('Tipo de emprendimiento actualizado correctamente.');

		return redirect(route('poderoso.types.index'));
	}

	/**
	 * Remove the specified types from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		if (Auth::user())
		{

			$types = $this->typesRepository->find($id);

			if(empty($types))
			{
				Flash::error('Tipo de emprendimiento no encontrado');

				return redirect(route('poderoso.types.index'));
			}

			$this->typesRepository->delete($id);

			Flash::success('Tipo de emprendimiento eliminado correctamente');

			return redirect(route('poderoso.types.index'));
		}
		else{

			return redirect(route('entrepreneurships.index'));

		}		
	}
}

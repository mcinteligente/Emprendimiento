<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses'  => 'entrepreneurshipController@index',
    'as'    => 'entrepreneurship'
]);

Route::get('/all', [
    'uses'  => 'entrepreneurshipController@paginate',
    'as'    => 'all'
]);

// Authentication routes...
Route::get('login', [
    'uses'  => 'Auth\AuthController@getLogin',
    'as'    => 'login'
]);
Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', [
    'uses'  => 'Auth\AuthController@getLogout',
    'as'    => 'logout'
]);

// Registration routes...
Route::get('register', [ 
    'uses'  => 'Auth\AuthController@getRegister',
    'as'    => 'register'
]);
Route::post('register', 'Auth\AuthController@postRegister');


Route::resource('entrepreneurships', 'entrepreneurshipController');

Route::get('profile', [
    'uses'  => 'entrepreneurshipController@postEdit',
    'as'    => 'profile'
]);


Route::get('removeAccount', [
    'uses'  => 'entrepreneurshipController@removeAccount',
    'as'    => 'removeAccount'
]);

Route::get('terms', [
    'uses'  => 'entrepreneurshipController@terms',
    'as'    => 'terms'
]);

Route::get('sendEmail','Auth\PasswordController@getEmail');

Route::post('sendEmail', [ 
    'uses'  => 'Auth\PasswordController@postEmail',
    'as'    => 'sendEmail'
]);
// Route::post('sendEmail','Auth\PasswordController@postEmail');
Route::get('/password/reset/{token}','Auth\PasswordController@getReset');

Route::post('resetPassword/{token}', [ 
    'uses'  => 'Auth\PasswordController@postReset',
    'as'    => 'resetPassword'
]);


// API
Route::group(['prefix' => 'poderoso'], function()
{
    Route::get('/', [
        'uses'  => 'entrepreneurshipController@admin',
        'as'    => 'list'
    ]);

    Route::resource('entrepreneurships', 'entrepreneurshipController');


    Route::get('entrepreneurships/{id}/delete', [
        'uses' => 'entrepreneurshipController@destroy',
        'as' => 'poderoso.entrepreneurships.delete',
    ]);

    Route::get('editentrep/{id}', [ 
        'uses'  => 'entrepreneurshipController@edit',
        'as'    => 'editentrep'
    ]);

    Route::post('editentrep/{id}', [
        'uses'  => 'entrepreneurshipController@update',
        'as'    => 'editentrep'
    ]);



    Route::resource('categories', 'categoriesController');

    Route::get('categories/{id}/delete', [
        'as' => 'poderoso.categories.delete',
        'uses' => 'categoriesController@destroy',
    ]);

    Route::post('categories', [
        'as' => 'categories',
        'uses' => 'categoriesController@store',
    ]);

    Route::resource('cities', 'citiesController');

    Route::get('cities/{id}/delete', [
        'as' => 'poderoso.cities.delete',
        'uses' => 'citiesController@destroy',
    ]);

    Route::post('cities', [
        'as' => 'cities',
        'uses' => 'citiesController@store',
    ]);

    Route::resource('communes', 'communesController');

    Route::get('communes/{id}/delete', [
        'as' => 'poderoso.communes.delete',
        'uses' => 'communesController@destroy',
    ]);

    Route::post('communes', [
        'as' => 'communes',
        'uses' => 'communesController@store',
    ]);

    Route::resource('types', 'typesController');

    Route::get('types/{id}/delete', [
        'as' => 'poderoso.types.delete',
        'uses' => 'typesController@destroy',
    ]);

    Route::post('types', [
        'as' => 'types',
        'uses' => 'typesController@store',
    ]);
});
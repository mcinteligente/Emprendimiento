<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $table = "media";
    

	public $fillable = [
	    "name",
		"path",
		"size",
		"type"
	];
}

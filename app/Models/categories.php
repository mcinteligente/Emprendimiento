<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class categories extends Model
{
    
	public $table = "categories";
    

	public $fillable = [
	    "id",
		"name",
		"slug"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"name" => "string",
		"slug" => "string"
    ];

	public static $rules = [
	    
	];

   	public function entrepreneurships()
    {
        return $this->hasMany('App\Models\entrepreneurships','category_id');
    }

}

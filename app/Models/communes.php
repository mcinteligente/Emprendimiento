<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class communes extends Model
{
    
	public $table = "communes";
    

	public $fillable = [
	    "name",
		"slug"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
		"slug" => "string"
    ];

	public static $rules = [
	    "name" => "required"
	];

	public function entrepreneurships()
    {
        return $this->hasMany('App\Models\entrepreneurships','commune_id');
    }

}

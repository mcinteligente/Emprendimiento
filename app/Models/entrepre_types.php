<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class entrepre_types extends Model
{
    public $table = "entrepreneurship_types";
    

	public $fillable = [
	    "type_id",
		"entrepreneurship_id"
	];
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class entrepreneurship extends Model
{
    
	public $table = "entrepreneurship";
    

	public $fillable = [
	    "user_id",
		"name",
		"email",
		"description",
		"category_id",
		"city_id",
		"commune_id",
		"phone",
		"address",
		"social_media"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

	public static $rules = [
	];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
	public function categories()
    {
        return $this->belongsTo('App\Models\categories','category_id');
    }

    public function cities()
    {
        return $this->belongsTo('App\Models\cities','city_id');
    }
    
    public function communes()
    {
        return $this->belongsTo('App\Models\communes','commune_id');
    }
    
    public function entrepreneurship_types()
    {
        return $this->hasMany('App\Models\entrepreneurship_types', 'type_id');
    }

    public function media()
    {
        return $this->belongsTo('App\Models\Media','image_id');
    }

    public function scopeTerm($query, $term)
    {
        if(trim($term) != ""){
            $query->where(\DB::raw("CONCAT(name, address, description)"), 'LIKE', "%$term%");
        }
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class entrepreneurship_types extends Model
{
    public $table = "entrepreneurship_types";
    

	public $fillable = [
	    "type_id",
		"entrepreneurship_id"
	];


    public function entrepreneurship()
    {
        return $this->belongsTo('App\Models\entrepreneurship','entrepreneurship_id');
    }

    public function types()
    {
       return $this->hasOne('App\Models\types','id');
    }

}

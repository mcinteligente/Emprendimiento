<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class entrepreneurships extends Model
{
    
	public $table = "entrepreneurships";
    

	public $fillable = [
	    "id",
		"name",
		"description",
		"category_id",
		"initiative_type",
		"phone",
		"address"	
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"name" => "string",
		"description" => "string",
		"category_id" => "string",
		"initiative_type" => "string",
		"phone"=> "string",
		"address"=>"string"
    ];

	public static $rules = [
	    
	];

	public function categories()
    {
        return $this->belongsTo('App\Models\categories','category_id');
    }

    public function cities()
    {
        return $this->belongsTo('App\Models\categories','city_id');
    }

    public function types()
    {
        return $this->belongsTo('App\Models\categories','type_id');
    }

    public function communes()
    {
        return $this->belongsTo('App\Models\communes','commune_id');
    }

}

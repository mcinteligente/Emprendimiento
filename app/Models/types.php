<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class types extends Model
{
    
	public $table = "types";
    

	public $fillable = [
	    "name",
		"slug"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

	public static $rules = [
	];

	public function entrepreneurship()
    {
         return $this->belongsToMany('App\Models\entrepreneurship','entrepreneurship_types', 'type_id', 'entrepreneurship_id');
    }

}

<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class Helper {
   public static function makeSlug($str) {
     return Str::slug($str);
   }
}
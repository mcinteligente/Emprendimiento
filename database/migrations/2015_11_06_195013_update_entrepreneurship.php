<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEntrepreneurship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entrepreneurship', function (Blueprint $table) {
            $table->string('email')->after('phone');
            $table->enum('publish', ['0', '1'])->default('1')->after('social_media');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entrepreneurship', function (Blueprint $table) {
            //
        });
    }
}

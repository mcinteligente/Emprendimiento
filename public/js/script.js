function main() {
	var _self = this;
	this.__constructor = function(){
		//Función que inicializa nuestra clase main
		console.log('Función que inicializa nuestra clase main');
		$('#goToWelcome, #reg, #reg2').click(function(){
			$('.welcome').show();
			$('#step1').hide();
			$('#step2').hide();
			$('#step3').hide();
		});
		$('#goToSS').click(function(){
			_self.goToId('search_section');
		});
		$('#goToRegister').click(function(){
			_self.goToRegister();
		});
		$('#goToStep1').click(function(){
			_self.goToRegisterStep1();
		});
		$('#goToStep2').click(function(){
			_self.goToRegisterStep2();
		});
		$('#register').submit(function(e){
			// _self.goToRegisterStep3(e);
		});
		var toresize = '.cover';
		if(!$(toresize).hasClass('intent')){
			$(window).resize(function() {
				$width = $(window).width();
				$(toresize).height($(window).height() + 70);
			});
			$(window).trigger('resize');
		}
		$('#openPopProfile').click(function(){
			$(".box-pophoverUserLog").show('fast');
		});
		$(".box-pophoverUserLog").mouseleave(function() {
			$(this).hide('fast');
		});
	};
	this.goToRegister = function(){
		$('.welcome').hide();
		$('#step1').show();
		var inputs = "#u_username, #u_lastname, #u_email, #u_password, #u_password_confirmation";
		$(inputs).bind('DOMAttrModified textInput input change keypress paste focus',function(data) {
			if($('#u_username').val() !== "" && $('#u_lastname').val() !== "" && $('#u_email').val() !== "" && $('#u_password').val() !== "" && $('#u_password_confirmation').val() !== ""){
				if ($('#u_password').val() === $('#u_password_confirmation').val()) {
					$('#goToStep1').prop("disabled", false);
				} else{
					$('#goToStep1').prop("disabled", true);
				}
			} else {
				$('#goToStep1').prop("disabled", true);
			}
		});
	};
	this.goToRegisterStep1 = function(){
		$('#step1').hide();
		$('#step2').show();
		var inputs = "#e_name, #e_description, #e_category, #e_type";
		$(inputs).bind('DOMAttrModified textInput input change keypress paste focus',function(data) {
			if($('#e_name').val() !== "" && $('#e_description').val() !== "" && $('#e_category').val() !== null/* && $('#e_type').val() !== null*/){
				$('#goToStep2').prop("disabled", false);
			} else {
				$('#goToStep2').prop("disabled", true);
			}
			var val = $('#e_description').val();
			var len = val.length;
			if (len >= 600) {
				val.value = val.value.substring(0, 500);
			} else {
				$('.help-block').text(600 - len);
			}
		});
	};
	this.goToRegisterStep2 = function(){
		$('#step2').hide();
		$('#step3').show();
		$(document).on('change', '.btn-file :file', function() {
			var input = $(this), numFiles = input.get(0).files ? input.get(0).files.length : 1, label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});

		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
			var input = $('.file_f').val(), log = numFiles > 1 ? numFiles + ' files selected' : label;
			if( input.length ) {
				$('.file_txt').val(log);
			} else {
				if( log ) alert(log);
			}
		});

		var inputs = "#e_email, #e_phone, #e_tyc, #e_city";
		$(inputs).bind('DOMAttrModified textInput input change keypress paste focus',function(data) {
			if($('#e_email').val() !== "" && $('#e_phone').val() !== "" && $('#e_city').val() !== null && $('#e_tyc').is(':checked')){
				$('#goToStep3').prop("disabled", false);
			} else {
				$('#goToStep3').prop("disabled", true);
			}

			if($('#e_city').val() != 39){
				$('#e_commune').parent().parent().hide();
			}
			else{
				$('#e_commune').parent().parent().show();
			}
		});
	};
	this.goToRegisterStep3 = function(e){
		e.preventDefault();
		var Type=$("#type").val();
		console.log(Type);
	};
	this.goToId = function(id){
		$('html, body').animate({scrollTop:$('#' + id).position().top}, 'slow');
	};
}

$(document).ready(function(){
	var _main = new main();
	_main.__constructor();
});
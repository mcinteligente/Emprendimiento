<?php
return [
	'error_title'		=> 'Please correct the following errors',
	'login_title'		=> 'Login',
	'login_button'		=> 'Login',
	'register_title'	=> 'Datos personales',
	'register_button'	=> 'Enviar',
	'entrepreneurship'	=> 'Emprendimiento',
	'next'	=> 'Siguiente',
];
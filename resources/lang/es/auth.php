<?php
return [
	'error_title'		=> 'Por favor corrige los siguientes errores',
	'login_title'		=> 'Identificarse',
	'login_button'		=> 'Ingresar',
	'register_title'	=> 'Registro',
	'register_button'	=> 'Enviar',
	'failed'			=> 'El usuario y la contraseña no coinciden'
];
<!-- Modal -->
<div class="container">
    <div class="modal fade register" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <h3 class="text-center col-md-8 col-md-offset-2">Entrar</h3>
                    <label class="text-center col-md-8 col-md-offset-2">Ingresa tus datos personales</label>
                    <div class="panel-body">
                        @include('partials/errors')

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <input name="email" type="email" value="{{ old('email') }}" class="form-control" placeholder="{{ trans('validation.attributes.email') }}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <input name="password" type="password" class="form-control" placeholder="{{ trans('validation.attributes.password') }}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                                        Ingresar
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <a href="{{ route('sendEmail') }}">¿Olvidaste tu contraseña?</a> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
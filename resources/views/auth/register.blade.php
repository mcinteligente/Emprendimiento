<!-- Modal -->
<div class="container">
    <div class="modal fade register" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    
                <article>
                    <div class="welcome">
                        <p>Esta es la oportunidad para dar a conocer tu iniciativa y generar alianzas con otros <strong>emprendedores</strong>.</p>
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-primary" id="goToRegister">
                                    Continuar
                                </button>
                            </div>
                        </div>
                    </div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}" id="register" enctype="multipart/form-data">
                        <div id="step1">
                            <h3 class="text-center col-md-8 col-md-offset-2">Cuéntanos sobre ti</h3>
                            @include('partials/errors')
                            <div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="text" required class="form-control" id="u_username" name="username" value="{{ old('username') }}" placeholder="Nombre">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="text" required class="form-control" id="u_lastname" name="lastname" value="{{ old('lastname') }}" placeholder="@lang('validation.attributes.lastname')">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="email" required class="form-control" id="u_email" name="email" value="{{ old('email') }}" placeholder="Correo electrónico">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="password" required class="form-control" id="u_password" name="password" placeholder="@lang('validation.attributes.password')">
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="password" required class="form-control" id="u_password_confirmation" name="password_confirmation" placeholder="@lang('validation.attributes.password_confirm')">
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="button" class="btn btn-primary" id="goToStep1">
                                            Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="step2">
                            <h3 class="text-center">Cuéntanos sobre tu iniciativa</h3>
                            <label class="text-center col-md-8 col-md-offset-2">Esta es la información que usaran otros emprendedores para contactarse contigo</label>
                            <div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="text" required class="form-control" id="e_name" name="e_name" value="{{ old('e_name') }}" placeholder="Nombre del emprendimiento">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <textarea class="form-control" required id="e_description" name="description" value="{{ old('description') }}" rows="4" placeholder="@lang('validation.attributes.description')" maxlength="600"></textarea>
                                        <p class="help-block">600</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <select class="form-control" id="e_category" name="category" value="{{ old('category') }}" required>
                                            <option selected disabled value="">Elije una categoría</option>
                                             @foreach($cats as $cat)
                                             <option value="{!! $cat->id !!}">
                                                {!! $cat->name !!}
                                             </option>   
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <select class="form-control" id="e_type" name="type" value="{{ old('type') }}" multiple required>
                                            <option disabled value="">Elije un tipo de iniciativa</option>
                                             @foreach($typs as $typ)
                                             <option value="{!! $typ->id !!}">
                                                {!! $typ->name !!}
                                             </option>   
                                            @endforeach
                                        </select>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="button" class="btn btn-primary" id="goToStep2" disabled>
                                            Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="step3">
                            <h3 class="text-center">Cuéntanos sobre tu iniciativa</h3>
                            <label class="text-center col-md-8 col-md-offset-2">Esta es la información que encontraran otros emprendedores para conocer tu emprendimiento</label>
                            <div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <select class="form-control" id="e_city" name="city" value="{{ old('city') }}" required>
                                            <option selected disabled>Elije una ciudad</option>
                                             @foreach($cits as $cit)
                                             <option value="{!! $cit->id !!}">
                                                {!! $cit->name !!}
                                             </option>   
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                   <div class="col-md-8 col-md-offset-2">
                                        <select class="form-control" id="e_commune" name="commune" value="{{ old('commune') }}">
                                            <option selected disable value="0">Elije una comuna</option>
                                             @foreach($coms as $com)
                                             <option value="{!! $com->id !!}">
                                                {!! $com->name !!}
                                             </option>   
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="email" required class="form-control" id="e_email" name="e_email" value="{{ old('e_email') }}" placeholder="@lang('validation.attributes.email')">
                                    </div>
                                </div>
                                <div class="form-group">
                                     <div class="col-md-8 col-md-offset-2">
                                        <input type="phone" required class="form-control" id="e_phone" name="phone" value="{{ old('phone') }}" placeholder="@lang('validation.attributes.phone')">
                                    </div>
                                </div>

                                <div class="form-group">
                                     <div class="col-md-8 col-md-offset-2">
                                        <input type="text" class="form-control" id="e_address" name="address" value="{{ old('address') }}" placeholder="@lang('validation.attributes.address')" />
                                    </div>
                                </div>
                                <div class="form-group">
                                     <div class="col-md-8 col-md-offset-2">
                                        <input type="text" class="form-control" id="e_social_web" name="social_web" value="{{ old('social_web') }}" placeholder="Página web o red social" />
                                    </div>
                                </div>
                                <div class="form-group">
                                     <div class="col-md-8 col-md-offset-2 input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                Adjuntar imagen <input type="file" class="file_f" id="e_image" name="image" value="{{ old('image') }}" accept="image/x-png, image/gif, image/jpeg" />
                                            </span>
                                        </span>
                                        <input type="text" class="form-control file_txt" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input type="checkbox" id="e_tyc" name="tyc" />
                                        <label for="">He leido y acepto <a href="{{ route('terms') }}" target="_blank">términos y condiciones</a></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary" id="goToStep3" disabled>
                                            Finalizar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </article>

                </div>

                </div>
            </div>
        </div>
    </div>
    
</div>
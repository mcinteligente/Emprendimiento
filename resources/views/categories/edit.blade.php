@extends('layout')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($categories, ['route' => ['poderoso.categories.update', $categories->id], 'method' => 'patch']) !!}

        @include('categories.fields')

    {!! Form::close() !!}
</div>
@endsection

<table class="table">
    <thead>
    <th>Id</th>
			<th>Name</th>
			<th>Slug</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($categories as $categories)
        <tr>
            <td>{!! $categories->id !!}</td>
			<td>{!! $categories->name !!}</td>
			<td>{!! $categories->slug !!}</td>
            <td>
                <a href="{!! route('poderoso.categories.edit', [$categories->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('poderoso.categories.delete', [$categories->id]) !!}" onclick="return confirm('¿Está seguro que desea eliminar esta categoría?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

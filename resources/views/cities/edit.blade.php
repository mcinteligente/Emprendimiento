@extends('layout')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($cities, ['route' => ['poderoso.cities.update', $cities->id], 'method' => 'patch']) !!}

        @include('cities.fields')

    {!! Form::close() !!}
</div>
@endsection

<table class="table">
    <thead>
    <th>Name</th>
			<th>Slug</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($cities as $cities)
        <tr>
            <td>{!! $cities->name !!}</td>
			<td>{!! $cities->slug !!}</td>
            <td>
                <a href="{!! route('poderoso.cities.edit', [$cities->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('poderoso.cities.delete', [$cities->id]) !!}" onclick="return confirm('¿Está seguro que desea eliminar esta ciudad?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

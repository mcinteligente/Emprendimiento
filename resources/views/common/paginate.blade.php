<div class="row">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
        <span class="pull-right">
    		{!! str_replace('/?', '?', $records->render()) !!}
        </span>
    </div>
</div>

@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'communes.store']) !!}

        @include('communes.fields')

    {!! Form::close() !!}
</div>
@endsection

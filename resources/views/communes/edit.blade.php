@extends('layout')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($communes, ['route' => ['poderoso.communes.update', $communes->id], 'method' => 'patch']) !!}

        @include('communes.fields')

    {!! Form::close() !!}
</div>
@endsection

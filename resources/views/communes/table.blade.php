<table class="table">
    <thead>
    <th>Name</th>
			<th>Slug</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($communes as $communes)
        <tr>
            <td>{!! $communes->name !!}</td>
			<td>{!! $communes->slug !!}</td>
            <td>
                <a href="{!! route('poderoso.communes.edit', [$communes->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('poderoso.communes.delete', [$communes->id]) !!}" onclick="return confirm('¿Está seguro que desea eliminar esta comuna?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

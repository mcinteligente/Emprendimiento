@if(Request::path() === '/')
<section class="cover">
@else
<section class="cover intent">
@endif

	<div class="portal-bottom-bar">
		<div class="container">
			<div class="row">
				<div class="col-xs-6">
					<div class="mde-logo"><a href="/"><img src="{!! url('/') !!}/img/logo-mde.png"></a></div>
				</div>
				<div class="col-xs-6 hidden-xs">
					<div class="alcaldia-logo text-right"><img src="{!! url('/') !!}/img/alcaldia.png"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="submenu">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-8 col-xs-0"></div>
				<div class="col-lg-3 col-md-4 col-xs-12">
					<ul class="row" ng-hide="user.session">
						@if (Auth::guest())
						<li class="col-md-5 col-xs-5"><a data-toggle="modal" data-target="#modalLogin" href="">Entrar</a></li>
						<li class="col-md-6 col-xs-6"><a id="reg" data-toggle="modal" data-target="#modalRegister" href="">Regístrate</a></li>
						@else
						<li class="col-md-12 col-xs-12"><a id="openPopProfile">{{ Auth::user()->name }} {{ Auth::user()->lastname }}</a></li>
						@endif
					</ul>
					<div class="box-pophoverUserLog">
						<a href="{{ route('profile') }}">Edita tu iniciativa</a>
						<a href="{{ route('logout') }}" class="l-logoutUser"><i class="fa fa-power-off"></i> Cerrar sesión</a>
					</div>

				</div><!-- fin col-md-3 -->
			</div>
		</div>
	</div>
	@if(Request::path() === '/')
	<article class="description">
		<h2>Es hora de compartir tu emprendimiento</h2>
		@if (Auth::guest())
		<p>
			<a id="reg2" data-toggle="modal" data-target="#modalRegister" href=""><button class="btn btn-primary">Regístrate</button></a>
		</p>
		@endif
		<p>
			<a id="goToSS"><i class="glyphicon glyphicon-chevron-down hvr-icon-hang"></i></a>
		</p>
	</article>
	@endif
	<article class="search" id="search_section">
		<form action="{{ route('all') }}" method="GET" id="search" class="col-lg-4 col-md-4 col-xs-12 col-lg-offset-4 col-md-offset-4">
			<div class="inner-addon left-addon">
				<i class="glyphicon glyphicon-search"></i>
				<input type="text" value="{{ isset($term) ? $term : '' }}" class="form-control" placeholder="¿Buscas algún emprendimiento o iniciativa?" name="term" />
			</div>
		</form>
	</article>
</section>
@include('auth.login')
@include('auth.register')
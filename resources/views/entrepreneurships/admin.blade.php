@extends('layout')

@section('content')
@include('menu.admin_menu')
    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">entrepreneurships</h1>

        </div>

        <div class="row">
            @if($entrepreneurships->isEmpty())
                <div class="well text-center">No entrepreneurships found.</div>
            @else
                @include('entrepreneurships.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $entrepreneurships])


    </div>
@endsection
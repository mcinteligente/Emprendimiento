@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'entrepreneurships.store']) !!}

        @include('entrepreneurships.fields')

    {!! Form::close() !!}
</div>
@endsection

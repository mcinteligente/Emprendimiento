@extends('layout')

@section('content')
<br />
<br />
<br />
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('register', []) }}" enctype="multipart/form-data">
            @include('partials/errors')
            <div class="panel panel-default">
                <div class="panel-heading">Hola</div>
                <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.name')</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.description')</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="description" value="{{ old('description') }}" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.category')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category" value="{{ old('category') }}">
                                    <option selected disable>Elije una categoría</option>
                                     @foreach($cats as $cat)
                                     <option value="{!! $cat->id !!}">
                                        {!! $cat->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.type')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="type" value="{{ old('type') }}">
                                    <option selected disable>Elije un tipo de iniciativa</option>
                                     @foreach($typs as $typ)
                                     <option value="{!! $typ->id !!}">
                                        {!! $typ->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.city')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="city" value="{{ old('city') }}">
                                    <option selected disable>Elije una ciudad</option>
                                     @foreach($cits as $cit)
                                     <option value="{!! $cit->id !!}">
                                        {!! $cit->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.commune')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="commune" value="{{ old('commune') }}">
                                    <option selected disable>Elije una comuna</option>
                                     @foreach($coms as $com)
                                     <option value="{!! $com->id !!}">
                                        {!! $com->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.phone')</label>
                            <div class="col-md-6">
                                <input type="phone" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.address')</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.social_web')</label>
                            <div class="col-md-6">
                                <input type="url" class="form-control" name="social_web" value="{{ old('social_web') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.image')</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image" value="{{ old('image') }}">
                            </div>
                        </div>
                </div>
            </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-5">
                            <button type="submit" class="btn btn-primary">
                                @lang('auth.register_button')
                            </button>
                        </div>
                    </div>
            </form>

        </div>
    </div>
</div>
@endsection
@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($entrepreneurship, ['route' => ['entrepreneurships.update', $entrepreneurship->id], 'method' => 'patch']) !!}

        @include('entrepreneurships.fields')

    {!! Form::close() !!}
</div>
@endsection

@extends('layout')

@section('content')
<div class="container profile">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <article>
            	@if($entrepreneurship)
                {!! Form::model($entrepreneurship, ['route' => ['editentrep', $entrepreneurship->id], 'method' => 'post', 'class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div>
	                    @include('partials/errors')
	                    @include('flash::message')
                        <h3 class="text-center col-md-8 col-md-offset-2">@lang('auth.entrepreneurship')</h3>
                        <div>

								 <input type="hidden" name="_token" value="{{ csrf_token() }}">

                             <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <input type="text" class="form-control" name="e_name" value="{!! $entrepreneurship->name !!}" placeholder="Nombre del emprendimiento">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <textarea class="form-control" name="description" value="{!! $entrepreneurship->description !!}" rows="4" placeholder="@lang('validation.attributes.description')" maxlength="600">{!! $entrepreneurship->description !!}</textarea>
                                	<p class="help-block">Máximo 600 caracteres</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <select class="form-control" name="category" value="{!! $entrepreneurship->category !!}">
                                        <option disabled>Elije una categoría</option>
                                         @foreach($cats as $cat)
                                         @if ($entrepreneurship->category == $cat->id)
                                         <option value="{!! $cat->id !!}" selected>{!! $cat->name !!}</option>   
                                         @else
                                         <option value="{!! $cat->id !!}">{!! $cat->name !!}</option>
                                         @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

							<div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <select class="form-control" name="city" id="e_city" value="{!! $entrepreneurship->city !!}">
                                        <option disabled>Elije una ciudad</option>
                                         @foreach($cits as $cit)
	                                         @if ($entrepreneurship->city == $cit->id)
	                                         <option value="{!! $cit->id !!}" selected>{!! $cit->name !!}</option>  
	                                         @else
	                                         <option value="{!! $cit->id !!}">{!! $cit->name !!}</option>  
	                                         @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <select class="form-control" name="commune" id="e_commune" value="{!! $entrepreneurship->commune !!}">
                                        <option disabled>Elije una comuna</option>
                                         @foreach($coms as $com)
                                         	@if ($entrepreneurship->commune == $com->id)
	                                         <option value="{!! $com->id !!}" selected>{!! $com->name !!}</option>   
	                                         @else
	                                         <option value="{!! $com->id !!}">{!! $com->name !!}</option>  
	                                         @endif
                                         @endforeach   
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                 <div class="col-md-8 col-md-offset-2">
                                    <input type="phone" class="form-control" name="phone" value="{!! $entrepreneurship->phone !!}" placeholder="@lang('validation.attributes.phone')">
                                </div>
                            </div>
   							<div class="form-group">
                                 <div class="col-md-8 col-md-offset-2">
                                    <input type="phone" class="form-control" name="email" value="{!! $entrepreneurship->email !!}" placeholder="@lang('validation.attributes.email')">
                                </div>
                            </div>
                            <div class="form-group">
                                 <div class="col-md-8 col-md-offset-2">
                                    <input type="text" class="form-control" name="address" value="{!! $entrepreneurship->address !!}" placeholder="@lang('validation.attributes.address')">
                                </div>
                            </div>
                            <div class="form-group">
                                 <div class="col-md-8 col-md-offset-2">
                                    <input type="text" class="form-control" name="social_web" value="{!! $entrepreneurship->social_media !!}" placeholder="Sitio web o @lang('validation.attributes.social_web')">
                                </div>
                            </div>
                            @if($entrepreneurship->image != url() . "/")
							<div class="form-group img">
								<div class="col-md-8 col-md-offset-2">
									<img src="{!! $entrepreneurship->image !!}" />
								</div>
							</div>
							@endif
							<div class="form-group">
                                 <div class="col-md-8 col-md-offset-2">
                                    <input type="file" class="form-control" name="image" value="{{ old('image') }}">
                                </div>
                        	</div>
                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-5">
                                    <button type="submit" class="btn btn-primary" id="goToStep3">
                                        @lang('auth.register_button')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                @else
                <br>
                	<div class="well text-center">No se encontró tu iniciativa.</div>
                @endif
                <div class="row col-md-8 col-md-offset-2">
					<a href="{!! route('removeAccount') !!}" onclick="return confirm('Si elimina el emprendimiento también se eliminará la cuenta a la cuál esta relacionada el emprendimiento. \n¿Está seguro que desea eliminar el emprendimiento?')">Eliminar emprendimiento</a>
                	<br>
                	<br>
                </div>
            </article>
        </div>
    </div>
</div>
<script>
setTimeout(function(){
	var inputs = "#e_city";
	$(inputs).bind('change focus',function(data) {
		console.log('funciona');
		if($('#e_city').val() != 39){
			$('#e_commune').parent().parent().hide();
		}
		else{
			$('#e_commune').parent().parent().show();
		}
	});
},100);
</script>
@endsection

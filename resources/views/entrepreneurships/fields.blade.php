<!-- User Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('user_id', 'User Id:') !!}
	{!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('description', 'Description:') !!}
	{!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('category_id', 'Category Id:') !!}
	{!! Form::text('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('type_id', 'Type Id:') !!}
	{!! Form::text('type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('city_id', 'City Id:') !!}
	{!! Form::text('city_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Commune Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('commune_id', 'Commune Id:') !!}
	{!! Form::text('commune_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('image_id', 'Image Id:') !!}
	{!! Form::text('image_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('phone', 'Phone:') !!}
	{!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Adress Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Social Media Url Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('social_media', 'Social Media Url:') !!}
	{!! Form::text('social_media', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>

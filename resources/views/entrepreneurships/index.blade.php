@extends('layout')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="text-center">Últimas iniciativas</h1>
            <!-- <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('entrepreneurships.create') !!}">Add New</a> -->
        </div>

        <div class="row lista">
            @if($entrepreneurships->isEmpty())
                <div class="well text-center">No se encontraron emprendimientos</div>
            @else
                @include('entrepreneurships.list')
            @endif
        </div>
        <div class="row more">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <a href="{{ route('all') }}" class="pull-right">Ver todas</a>
            </div>
        </div>

    </div>
    <script type="text/javascript">
    setTimeout(function(){
        var Things = $('.error');
        for (var i = 0; i < Things.length; i++) {
            if($('.error')[i].innerHTML == "El usuario y la contraseña no coinciden"){
                $('#modalLogin').modal();
                break;
            } else{
                $('.welcome').hide();
                $('#step1').show();
                $('.alert').show();
                $('#modalRegister').modal();
                break;
            }
        };

        var inputs = "#u_username, #u_lastname, #u_email, #u_password, #u_password_confirmation";
        $(inputs).bind('DOMAttrModified textInput input change keypress paste focus',function(data) {
            if($('#u_username').val() !== "" && $('#u_lastname').val() !== "" && $('#u_email').val() !== "" && $('#u_password').val() !== "" && $('#u_password_confirmation').val() !== ""){
                if ($('#u_password').val() === $('#u_password_confirmation').val()) {
                    $('#goToStep1').prop("disabled", false);
                } else{
                    $('#goToStep1').prop("disabled", true);
                }
            } else {
                $('#goToStep1').prop("disabled", true);
            }
        });
    },100);
    </script>
@endsection
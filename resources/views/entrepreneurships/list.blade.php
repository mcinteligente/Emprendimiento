@foreach($entrepreneurships as $entrepreneurship)
	<div class="entre col-xs-12 col-md-8 col-md-offset-2">
		<div class="col-md-2 col-md-offset-1 img">
			<img src="{!! $entrepreneurship->image !!}" />
		</div>
		<div class="col-xs-12 col-md-8">
			<h3>{!! $entrepreneurship->name !!}</h3>
			<strong>{!! $entrepreneurship->category !!}</strong>
			<p>{!! $entrepreneurship->short_description !!}</p>
			<span class="label label-default" data-toggle="modal" data-target="#modal{!! $entrepreneurship->id !!}">Ver más</span>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade datailEntrepreneurship" id="modal{!! $entrepreneurship->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="info">
						<img src="{!! $entrepreneurship->image !!}" />
						<h3>{!! $entrepreneurship->name !!}</h3>
						<strong>{!! $entrepreneurship->category !!}</strong>
						<p>{!! $entrepreneurship->description !!}</p>
					</div>
					<div class="contact">
						<strong>Contacto</strong>
						<div>{!! $entrepreneurship->user->name !!} {!! $entrepreneurship->user->lastname !!}</div>
						<div>Tel: {!! $entrepreneurship->phone !!}</div>
						<div>Dirección: {!! $entrepreneurship->address !!} / {!! $entrepreneurship->city !!}</div>
						<div>{!! $entrepreneurship->email !!}</div>
						@if($entrepreneurship->social_media && $entrepreneurship->social_media != '')
						<div>Web: <a href="{!! $entrepreneurship->social_media !!}" target="_blank">{!! $entrepreneurship->social_media !!}</a></div>
						@endif
					</div>
				</div>
				<!-- <div class="modal-footer">
					<p class="pull-right">
						<strong>Compartir:</strong>
						<span>F</span>
						<span>T</span>
					</p>
				</div> -->
			</div>
		</div>
	</div>
@endforeach
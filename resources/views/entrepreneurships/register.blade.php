@extends('layout')

@section('content')
<br />
<br />
<br />
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @include('partials/errors')
            <div class="panel panel-default">
                <div class="panel-heading">@lang('auth.register_title')</div>
                <div class="panel-body">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.name')</label>
                            <div class="col-md-6">
                                <input type="text" required class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.email')</label>
                            <div class="col-md-6">
                                <input type="email" required class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.password')</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                       <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.password_confirm')</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirm">
                            </div>
                        </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">@lang('auth.entrepreneurship')</div>
                <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.name')</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.description')</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="description" value="{{ old('description') }}" rows="4"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.category')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category" value="{{ old('category') }}">
                                    <option selected disable>Elije una categoría</option>
                                     @foreach($cats as $cat)
                                     <option value="{!! $cat->id !!}">
                                        {!! $cat->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.category')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category" value="{{ old('category') }}">
                                    <option selected disable>Elije un tipo de iniciativa</option>
                                     @foreach($cats as $cat)
                                     <option value="{!! $cat->id !!}">
                                        {!! $cat->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.type')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category" value="{{ old('category') }}">
                                    <option selected disable>Elije una categoría</option>
                                     @foreach($cats as $cat)
                                     <option value="{!! $cat->id !!}">
                                        {!! $cat->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.category')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category" value="{{ old('category') }}">
                                    <option selected disable>Elije una categoría</option>
                                     @foreach($cats as $cat)
                                     <option value="{!! $cat->id !!}">
                                        {!! $cat->name !!}
                                     </option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.initiative_type')</label>
                            <div class="col-md-6">
                                <select class="form-control" name="initiative_type" value="{{ old('initiative_type') }}">
                                    <option selected disable>Elije un tipo</option>
                                    <option value="Servicio">Servicio</option>
                                    <option value="Producto">Producto</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.phone')</label>
                            <div class="col-md-6">
                                <input type="phone" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('validation.attributes.address')</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                            </div>
                        </div>
                </div>
            </div>
                    <div class="form-group">
                        <div class="col-md-12 col-md-offset-5">
                            <button type="submit" class="btn btn-primary">
                                @lang('auth.register_button')
                            </button>
                        </div>
                    </div>
            </form>

        </div>
    </div>
</div>
@endsection
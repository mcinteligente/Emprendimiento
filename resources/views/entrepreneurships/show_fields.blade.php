<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $entrepreneurship->user_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $entrepreneurship->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $entrepreneurship->description !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $entrepreneurship->category_id !!}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{!! $entrepreneurship->type_id !!}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{!! $entrepreneurship->city_id !!}</p>
</div>

<!-- Commune Id Field -->
<div class="form-group">
    {!! Form::label('commune_id', 'Commune Id:') !!}
    <p>{!! $entrepreneurship->commune_id !!}</p>
</div>

<!-- Image Id Field -->
<div class="form-group">
    {!! Form::label('image_id', 'Image Id:') !!}
    <p>{!! $entrepreneurship->image_id !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $entrepreneurship->phone !!}</p>
</div>

<!-- Adress Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $entrepreneurship->address !!}</p>
</div>

<!-- Social Media Url Field -->
<div class="form-group">
    {!! Form::label('social_media_url', 'Social Media Url:') !!}
    <p>{!! $entrepreneurship->social_media_url !!}</p>
</div>


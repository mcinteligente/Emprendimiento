<table class="table">
    <thead>
    <th>Id</th>
			<th>Name</th>
			<th>Description</th>
			<th>Category</th>
			<th>City</th>
			<th>Commune</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Adress</th>
			<th>Social Media Url</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>

    @foreach($entrepreneurships as $entrepreneurship)
        <tr>
            <td>{!! $entrepreneurship->user_id !!}</td>
			<td>{!! $entrepreneurship->name !!}</td>
			<td>{!! $entrepreneurship->short_description !!}</td>
            <td>{!! $entrepreneurship->category !!}</td>
			<td>{!! $entrepreneurship->city !!}</td>
			<td>{!! $entrepreneurship->commune !!}</td>
			<td>{!! $entrepreneurship->phone !!}</td>
			<td>{!! $entrepreneurship->email !!}</td>
			<td>{!! $entrepreneurship->address !!}</td>
			<td>{!! $entrepreneurship->social_media !!}</td>
            <td>
                <a href="{!! route('poderoso.entrepreneurships.edit', [$entrepreneurship->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('poderoso.entrepreneurships.delete', [$entrepreneurship->id]) !!}" onclick="return confirm('Si elimina el emprendimiento también se eliminará la cuenta a la cuál esta relacionada el emprendimiento. \n¿Está seguro que desea eliminar el emprendimiento?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
  
    </tbody>
</table>

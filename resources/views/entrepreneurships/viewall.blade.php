@extends('layout')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            @if($term != '')
            <h1 class="text-center">Búsqueda: <i>{!! $term !!}</i></h1>
            <div class="text-center">
                <a href="{{ route('all') }}">Ver todas</a>  
            </div>
            @else
            <h1 class="text-center">Todas las iniciativas</h1>
            @endif
        </div>

        <div class="row lista">
            @if($entrepreneurships->isEmpty())
                <div class="well text-center">No se encontraron iniciativas.</div>
            @else
                @include('entrepreneurships.list')
            @endif
        </div>

        @include('common.paginate', ['records' => $entrepreneurships])
        <script type="text/javascript">
    setTimeout(function(){
        var Things = $('.error');
        for (var i = 0; i < Things.length; i++) {
            if($('.error')[i].innerHTML == "El usuario y la contraseña no coinciden"){
                $('#modalLogin').modal();
                break;
            } else{
                $('.welcome').hide();
                $('#step1').show();
                $('.alert').show();
                $('#modalRegister').modal();
                break;
            }
        };
    },100);
    </script>
    </div>
@endsection
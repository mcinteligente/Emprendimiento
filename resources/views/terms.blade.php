@extends('layout')

@section('content')

	<div class="container">

		@include('flash::message')

		<div class="row">
			<h1 class="text-center">Términos y condiciones</h1>
		</div>

		<div class="row">
			<div class="container-fluid">
				<p>Con el objetivo de generar una interacción activa y permanente con nuestros usuarios, queremos informarte que el Programa Medellín Ciudad Inteligente del Municipio de Medellín, a través de su plataforma web, y actuando como responsable de la información, podrá recopilar ciertos datos como tu nombre, apellido, correo electrónico, ciudad de residencia, teléfono de contacto, tipo y número de documento, intereses, biografía y fotografía del perfil. Además, podrá solicitarte la creación de un nombre de usuario y contraseña como medida de seguridad.</p>
				<p>Así nos comprometemos, como Programa Medellín Ciudad Inteligente del Municipio de Medellín, a proteger la seguridad de la información personal que nos suministras, al utilizar tecnologías y procedimientos que impidan el acceso, revelación y usos no autorizados de la misma.</p>
				<p>Te recordamos que siempre utilizaremos tus datos (y en especial aquellos suministrados por usuarios menores de edad con autorización de sus padres o tutores) de acuerdo a los principios constitucionales y respetando tus derechos fundamentales.</p>
				<p>Es importante que conozcas que el Programa Medellín Ciudad Inteligente del Municipio de Medellín recopila y utiliza tu información personal para la administración derivada de los servicios que ofrece su plataforma web. Igualmente, lo hace para ofrecerte servicios de información como, por ejemplo, para que te enteres de los contenidos que tenemos disponibles para ti.</p>
				<p>Por esto, es fundamental que sepas que el Programa Medellín Ciudad Inteligente del Municipio de Medellín no pone a la venta, alquila, ni arrienda a terceros los datos de los usuarios consignados en sus páginas web, aplicaciones o redes sociales.</p>
				<p>Sin embargo, es posible que en ocasiones nos pongamos en contacto contigo, en nombre de aliados externos a nuestras iniciativas, con el fin de brindarte información sobre ofertas concretas que puedan ser de tu interés, siempre vinculadas al objeto de esta plataforma web. Te aclaramos que en estos casos nunca se le transferirán tus datos personales (descritos anteriomente) a ningún tercero o aliado.</p>
				<p>Otro punto que queremos reiterarte es que siempre garantizaremos el uso de la información que nos suministres de acuerdo con los principios constitucionales y respetando tus derechos fundamentales. Por eso, si recolectamos datos cuyo uso indebido pueda afectar tu intimidad (entre los que se encuentran datos biométricos como sexo, edad o estrato socioeconómico), te garantizamos que serán utilizados solo en nuestra base de datos para el funcionamiento de la plataforma web y para proporcionar servicios de información.</p>
				<p>La información que se te pida en ésta plataforma web, será usada únicamente para compartir con los asistentes al evento. Cualquier uso posterior de la misma, no será responsabilidad del Programa Medellín Ciudad Inteligente.</p>
				<p>Recuerda que siempre serás el único propietario de tu información, y por lo tanto, gozas de los derechos previstos en la Constitución y en la Ley, como el derecho a conocer, actualizar, rectificar y suprimir tu información personal de nuestra base de datos, así como el derecho a revocar el consentimiento que nos otorgaste para el tratamiento de tu información. La totalidad de tus derechos la puedes revisar en la <a href="http://www.secretariasenado.gov.co/senado/basedoc/ley_1581_2012.html">Ley 1581 de 2012</a> y en el <a href="http://www.sic.gov.co/documents/10165/1922233/DECRETO+1377+DEL+27+DE+JUNIO+DE+2013.pdf/17143142-b45b-44d7-b568-6accad6323aa">Decreto 1377 de 2013</a>.</p>
				<p>Si tienes cualquier inquietud relacionada con esta política, o quieres ejercer tus derechos como titular de la información (revocar los permisos concedidos, solicitar alguna modificación, corrección o supresión de todos los datos que has proporcionado, dejar de recibir información o darte de baja de nuestras bases de datos), comunícate con nosotros a través de alguno de los siguientes medios y explica tu inquietud. Siempre te atenderemos con gusto:</p>
				<p>Programa Medellín Ciudad Inteligente de la Alcaldía de Medellín. Visítanos o envíanos una carta a la carrera 43B #11- 10, piso 2, Medellín (Colombia). Llámanos al teléfono: (57) (4) 444 49 63. O escríbenos al correo electrónico: comunicaciones@mdeinteligente.co. A través de estos medios puedes dirigirte a Juan Diego Urrea, líder de Comunicación Pública de nuestro Programa.</p>
				<p>Esta política de protección de datos y tratamiento de información, y la base de datos recopilada, han entrado en vigencia a partir del 13 de noviembre de 2014.</p>
				<br>
				<br>
			</div>
		</div>

    </div>
@endsection
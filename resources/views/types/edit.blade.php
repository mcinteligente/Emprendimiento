@extends('layout')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($types, ['route' => ['poderoso.types.update', $types->id], 'method' => 'patch']) !!}

        @include('types.fields')

    {!! Form::close() !!}
</div>
@endsection

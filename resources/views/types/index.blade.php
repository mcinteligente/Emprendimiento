@extends('layout')

@section('content')
@include('menu.admin_menu')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            Types
        </div>
        <div class="panel-body">
            <div class="row">
                @include('flash::message')
            </div>

            <div class="row">
                @if($types->isEmpty())
                    <div class="well text-center">Tipos de emprendimiento no encontrados.</div>
                @else
                <div class="col-lg-12">
                    @include('types.table')
                </div>
                @endif
            </div>

            @include('common.paginate', ['records' => $types])

        </div>
    </div>

    <form class="form-horizontal" role="form" method="POST" action="{{ route('types') }}">
        @include('partials/errors')
        <div class="panel panel-default">
            <div class="panel-heading">@lang('auth.register_title')</div>
            <div class="panel-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('validation.attributes.name')</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-md-offset-5">
                        <button type="submit" class="btn btn-primary">
                            @lang('auth.register_button')
                        </button>
                    </div>
                </div>
            </div>
        </div>
        
    </form>
</div>
@endsection
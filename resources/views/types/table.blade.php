<table class="table">
    <thead>
    <th>Name</th>
			<th>Slug</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($types as $types)
        <tr>
            <td>{!! $types->name !!}</td>
			<td>{!! $types->slug !!}</td>
            <td>
                <a href="{!! route('poderoso.types.edit', [$types->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('poderoso.types.delete', [$types->id]) !!}" onclick="return confirm('¿Está seguro que desea eliminar este tipo de emprendimiento?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
